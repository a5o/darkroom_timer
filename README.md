# README #

## Summary ##

Darkroom timer is an Arduino Uno compatible darkroom timer program supporting f-stop printing, automated test strip printing, burning and dodging.

## Required extra libraries ##

 * [Buxtronic Rotary library](https://github.com/buxtronix/arduino)
 * [New LiquidCrystal library](https://bitbucket.org/fmalpartida/new-liquidcrystal/)

## Construction scheme ##

![scheme](img/scheme.png)
