#include <Rotary.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <EEPROM.h>

//#define DEBUG 1

// input pins rotary encoder 1 
int DT = 2;
int CLK = 3;
int pushButton1 = 7; // exchanged with pushButton2!

// input pins rotary encoder 2 

int DT2 = 5;
int CLK2 = 6;
int pushButton2 = 4;

// buzzer pin
int buzzer = 8;

// main button pin

int startButton = 9;

// relay pins

int rel1 = 10;
int rel2 = 11;

// two way switch

int sw = A3;
bool focusing = false;

// timer counter
float counter = 1;
float last_counter = -1.0;
unsigned long start_time = 0;
int timec = 0;
bool running = false;

// strip test
int strip_step;
//char unsigned nsteps = 6;
//bool refresh_strip = false;

// precision counter
int counter2 = 1;
float precision_list[] = {1.0,3.0,6.0,9.0};

// burn/dodge counter
int counter3 = 1;
int last_counter3 = 0;
bool refresh_dodge = false;
float burn_increments[] = {1.0/9,1.0/6,2.0/9,1.0/3,4.0/9,3.0/6,5.0/9,2.0/3,7.0/9,8.0/9,1.0,
        1+1.0/9,1+1.0/6,1+2.0/9,1+1.0/3,1+4.0/9,1+3.0/6,1+5.0/9,1+2.0/3,1+7.0/9,1+8.0/9,2.0,
        2+1.0/9,2+1.0/6,2+2.0/9,2+1.0/3,2+4.0/9,2+3.0/6,2+5.0/9,2+2.0/3,2+7.0/9,2+8.0/9,3.0};
String burn_inc_labels[] = {"1/9","1/6","2/9","1/3","4/9","3/6","5/9","2/3","7/9","8/9","1.0",
        "1&1/9","1&1/6","1&2/9","1&1/3","1&4/9","1&3/6","1&5/9","1&2/3","1&7/9","1&8/9","2.0",
        "2&1/9","2&1/6","2&2/9","2&1/3","2&4/9","2&3/6","2&5/9","2&2/3","2&7/9","2&8/9","3.0"}; 

// dodge
bool dodge_finish = false;

// precisione f-stop
float precision = 1/precision_list[0];
float last_precision = -1.0;

// config
char confi = 0;
char last_confi = -1;
String conf_labels[] = {"Strip steps","Dry down","Set defaults"};
int conf_vals[] = {0,0,0};
bool refresh_config = false;

// pushbutton 1 last read
unsigned long lastread1 = 0;

// pushbutton 2 last read
unsigned long lastread2 = 0;

// pushbutton start last read
unsigned long lastread3 = 0;

// switchbutton last read
unsigned long lastread4 = 0;

// modes
int mode = 0;
int last_mode = -1;
int nmodes = 5;

// istanza Rotary per encoder 1
Rotary rotary1 = Rotary(DT, CLK);

// encoder 2
Rotary rotary2 = Rotary(DT2, CLK2);

LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE); 

void setup() {
  read_config();
  #ifdef DEBUG
  Serial.begin(9600);
  #endif
  lcd.begin(16, 2); 
  lcd.clear();
  lcd.setBacklight(HIGH);
  // attivo gli interrupt per l'encoder 1
  //attachInterrupt(0, rotate1, CHANGE);
  //attachInterrupt(1, rotate1, CHANGE);

  // metto i relay in modalità HIGH (disattivi)
  pinMode(rel1, OUTPUT);
  pinMode(rel2, OUTPUT);
  digitalWrite(rel1, HIGH);
  digitalWrite(rel2, HIGH);
  
  // attivo la resistenza di pullup sui
  // pushbutton
  pinMode(pushButton1, INPUT);
  digitalWrite(pushButton1, HIGH);
  pinMode(pushButton2, INPUT);
  digitalWrite(pushButton2, HIGH);
  pinMode(startButton, INPUT_PULLUP);
  pinMode(sw, INPUT_PULLUP);
}

void loop() {
  if (! running) {
    int reading = digitalRead(sw);
    if (reading == LOW) {
      if (! focusing) {
        #ifdef DEBUG
        Serial.println("Focusing");
        #endif
        digitalWrite(rel1, LOW);
        lcd.clear();
        lcd.setCursor(0, 0); 
        lcd.print("Focus");
      } 
      focusing = true;
    } else if (focusing) {
      digitalWrite(rel1, HIGH);
      focusing = false;
      last_mode = -1;
      #ifdef DEBUG
      Serial.println("End focusing");
      Serial.print("Mode: ");
      Serial.println(mode);
      #endif
    }
    if (focusing) {
      return;
    }
    button1();
    button2();
    button3();
    // lettura rotary encoders
    rotate1();
    rotate2();
  }
  switch (mode) {
    case 0: //normal mode
    {
      precision = 1/precision_list[counter2];
      String precision_label = String(precision_list[counter2]);
      precision_label.replace(".00","");
      if ((counter != last_counter) | (precision != last_precision) | (mode != last_mode)) { 
        last_counter = counter;
        last_precision = precision;
        last_mode = mode;
        lcd.clear();
        lcd.setCursor(0, 0); 
        lcd.print("Normal"); 
        lcd.setCursor( 0, 1 ); 
        lcd.print(counter);
        lcd.print(" f:1/");
        lcd.print(precision_label);
      }
      countdown();
      break;  
    }
    case 1: // strip mode
    {
      precision = 1/precision_list[counter2];
      String precision_label = String(precision_list[counter2]);
      precision_label.replace(".00","");
      float fstops[conf_vals[0]];
      calc_fstops(conf_vals[0],precision,counter,fstops);
      if ((counter != last_counter) | (precision != last_precision) | (mode != last_mode)) {// | (refresh_strip)) {
        last_counter = counter;
        last_precision = precision;
        last_mode = mode;
        //refresh_strip = false;
        #ifdef DEBUG
        Serial.println("F-stops:");
        for (int i=0;i<conf_vals[0];i++) {
          Serial.println(fstops[i]);
        }
        #endif
        lcd.clear();
        lcd.setCursor(0, 0); 
        lcd.print("Strip n=");
        lcd.print(conf_vals[0]);
        lcd.print(" f:1/"); 
        lcd.print(precision_label);
        lcd.setCursor( 0, 1 ); 
        lcd.print(fstops[0]);
        lcd.print(" -> ");
        lcd.print(fstops[conf_vals[0]-1]);
        //strip_step = 0;
      } 
      strip(fstops,conf_vals[0]);
      break;
    } 
    case 2: //burn mode
    {
      if ((counter3 != last_counter3) | (mode != last_mode)) {
        last_counter3 = counter3;
        last_mode = mode;
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("Burn +");
        lcd.print(burn_inc_labels[counter3]);
        lcd.setCursor(0, 1);
        lcd.print(counter);
        lcd.print(" -> ");
        lcd.print(counter*pow(2,burn_increments[counter3]));
      }
      burn();
      break;
    }
    case 3: //dodge mode
    {
      if ((counter3 != last_counter3) | (mode != last_mode) | (refresh_dodge)) {
        last_counter3 = counter3;
        last_mode = mode;
        refresh_dodge = false;
        lcd.clear();
        lcd.setCursor(0, 0);
        if (dodge_finish) {
          lcd.print("Insert dodger");
        } else {
          lcd.print("Dodge -");
          lcd.print(burn_inc_labels[counter3]);
        }
        lcd.setCursor(0, 1);
        if (dodge_finish) {
          lcd.print("START to finish");
        } else {
          lcd.print(counter/pow(2,burn_increments[counter3]));
          lcd.print(" -> ");
          lcd.print(counter);  
        }
      }
      dodge();
      break;      
    }
    case 4: //config mode
    {
      if ((confi != last_confi) | (mode != last_mode) | (refresh_config)) {
        refresh_config = false;
        if (confi == 0) { // strip steps
          if (conf_vals[confi] < 2) {
            conf_vals[confi] = 2;
          }else if (conf_vals[confi] > 10) {
            conf_vals[confi] = 10;
          }
        }else if (confi == 1) { // dry down between 0 and 15
          if (conf_vals[confi] <0) {
            conf_vals[confi] = 0;
          }else if (conf_vals[confi] >15) {
            conf_vals[confi] = 15;
          }
        }
        last_confi = confi;
        last_mode = mode;
        if (confi == 2) {
          lcd.clear();
          lcd.setCursor(0, 0);
          lcd.print("To set defaults");
          lcd.setCursor(0, 1);
          lcd.print("press START");
        }else{
          lcd.clear();
          lcd.setCursor(0, 0);
          lcd.print("Config");
          lcd.setCursor(0, 1);
          lcd.print(conf_labels[confi]);
          lcd.print(": ");
          lcd.print(conf_vals[confi]);
          if (confi == 1) {
            lcd.print("%");
          }
        }
      }
      break;
    }
  }
}


void calc_fstops(int steps, float increment, float basetime,float *fstops) {
  int i;
  float time;
  time = basetime;
  fstops[0] = time;
  for (i=1;i<steps;i++) {
    time = time*pow(2,increment);
    fstops[i] = time;
  }
}

void strip(float *fstops, int n){
  unsigned long current_time = millis(); 
  if (running && strip_step <= n && (current_time - start_time >= fstops[strip_step]*1000)) {
    #ifdef DEBUG
    Serial.println(fstops[strip_step]);
    #endif
    strip_step++;
    if (strip_step < n) {
      tone(buzzer, 1319, 50);
      #ifdef DEBUG
      Serial.print("Step ");
      Serial.print(strip_step);
      Serial.println(" completed");
      #endif
    }else {
      digitalWrite(rel1,HIGH);
      #ifdef DEBUG
      Serial.println("END");
      #endif
      tone(buzzer, 1760, 150);
      running = false;
      strip_step = 0;
    }
  }
}

void burn() {
  unsigned long current_time = millis();
  if (running && ((current_time - start_time >= timec))) {
    digitalWrite(rel1, HIGH);
    #ifdef DEBUG
    Serial.println("END");
    #endif
    tone(buzzer, 1760, 150);
    running = false;
  }
}

void dodge() {
  unsigned long current_time = millis();
  if (running && ((current_time - start_time >= timec))) {
    digitalWrite(rel1, HIGH);
    #ifdef DEBUG
    Serial.println("END");
    #endif
    running = false;
    if (dodge_finish) {
      tone(buzzer, 1760, 150);
      dodge_finish = false;
      refresh_dodge = true;
    }else{
      tone(buzzer, 1319, 50);
      dodge_finish = true;
      lcd.clear();
      lcd.setCursor(0, 0); 
      lcd.print("Insert dodger");
      lcd.setCursor( 0, 1 );
      lcd.print("START to finish");
    }
  }
}

void countdown() {
  unsigned long current_time = millis();
  if (running && ((current_time - start_time >= timec))) {  
    digitalWrite(rel1, HIGH);
    #ifdef DEBUG
    Serial.println("END");
    #endif
    tone(buzzer, 1760, 150);
    running = false;
  }
}

void button1() {
    // lettura bottone encoder 1
    int reading1 = digitalRead(pushButton1);
    if (reading1 == LOW){
      // debouncing
      if ((millis() - lastread1) > 250){
        mode += 1;
        if (mode > nmodes-1) {
          mode = nmodes-1;
        }
        lastread1 = millis();
      }
    }
}

void button2() {
      // lettura bottone encoder 2
    int reading2 = digitalRead(pushButton2);
    if (reading2 == LOW){
      // debouncing
      if ((millis() - lastread2) > 250){
        mode -= 1;
        if (mode < 0){
          mode = 0;
        }
        lastread2 = millis();
      }
    }
}

void button3() {
    // lettura bottone start
    int reading3 = digitalRead(startButton);
    if (reading3 == LOW){
      // debouncing
      if ((millis() - lastread3) > 500){
        switch (mode) {
          case 0: // normal mode
          {
            timec = ((counter)*(100-conf_vals[1])/100)*1000; //correction for dry down
            running = true;
            #ifdef DEBUG
            Serial.println("Starting timer");
            #endif
            start_time = millis();
            digitalWrite(rel1, LOW);
            lastread3 = millis();
          }
            break;
          case 1: //strip mode
          {
            running = true;
            strip_step = 0;
            lastread3 = millis();
            #ifdef DEBUG
            Serial.println("Starting timer");
            #endif
            start_time = millis();
            digitalWrite(rel1,LOW);
          }
            break;
          case 2: //burn mode
          {
            #ifdef DEBUG
            Serial.println("Starting timer");
            #endif
            running = true;
            lastread3 = millis();
            timec = ((counter*pow(2,burn_increments[counter3])-counter)*(100-conf_vals[1])/100)*1000;
            start_time = millis();
            digitalWrite(rel1, LOW);
          }
            break;
          case 3: // dodge mode
          {
            #ifdef DEBUG
            Serial.println("Starting timer");
            #endif
            running = true;
            lastread3 = millis();
            if (dodge_finish) {
              timec = ((counter-counter/pow(2,burn_increments[counter3]))*(100-conf_vals[1])/100)*1000;
            }else{
              timec = ((counter/pow(2,burn_increments[counter3]))*(100-conf_vals[1])/100)*1000;
            }
            start_time = millis();
            digitalWrite(rel1, LOW);
            break;
          }
          case 4: //config mode
          {
            #ifdef DEBUG
            Serial.println("Saving configuration");
            #endif
            lastread3 = millis();
            if (confi == 2) {
              save_defaults();
              read_config();
            }else{
              EEPROM.write(confi,conf_vals[confi]); // only 100.000 cycles!!!
            }
            break;
          }
        }
      }
    }
}

// rotate is called anytime the rotary inputs change state.
void rotate1() {
  unsigned char result = rotary1.process();
  if (result == DIR_CW) {
    switch (mode){
      case 0:
      case 1:
        counter = counter*pow(2,precision);
        if (counter > 60) {
          counter = 60;
        }
        break;
      case 4:
        conf_vals[confi]++;
        refresh_config = true;
        break;
    }  
  } else if (result == DIR_CCW) {
    switch (mode){
      case 0:
      case 1:
        counter = counter/pow(2,precision);
        if (counter < 1) {
          counter = 1;
        }
        break;
      case 4:
        conf_vals[confi]--;
        refresh_config = true;
        break;
    }
  }
}

void rotate2(){
  unsigned char result = rotary2.process();
  if (result == DIR_CW) {
    switch (mode){
      case 0:
      case 1:
        counter2++;
        if (counter2 > 3) {
          counter2 = 3;
        }
        break;
      case 2:
      case 3:
        counter3++;
        if (counter3 > 32) {
          counter3 = 32;
        }
        break;
      case 4:
        confi++;
        if (confi > 2) {
          confi = 2;
        }
    }
  } else if (result == DIR_CCW) {
    switch (mode){
      case 0:
      case 1:
        counter2--;
        if (counter2 < 0) {
          counter2 = 0;
        }
        break;
      case 2:
      case 3:
        counter3--;
        if (counter3 < 0) {
          counter3 = 0;
        }
        break;
      case 4:
        confi--;
        if (confi < 0) {
          confi = 0;
        }
    }
  }   
}

void read_config () {
  int v1 = EEPROM.read(0);
  int v2 = EEPROM.read(1);
  conf_vals[0] = v1;
  conf_vals[1] = v2;
}

void save_defaults () {
  int v1 = 6;
  int v2 = 0;
  EEPROM.write(0,v1);
  EEPROM.write(1,v2);
}
